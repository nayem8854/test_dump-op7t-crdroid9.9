#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus7T.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus7T-user \
    lineage_OnePlus7T-userdebug \
    lineage_OnePlus7T-eng
